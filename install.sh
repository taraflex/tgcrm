#!/bin/bash

apt-get update -y 
apt-get upgrade --yes --force-yes 
apt-get remove apache2 -y 
apt-get install git lsof htop ufw socat nginx postgresql postgresql-contrib curl php-cli php-fpm php-curl php-pgsql php-zip php-dom php-gd php-mbstring php-dev unzip openssl libssl-dev procps -y

cd /tmp

# swoole service
yes | pecl install swoole && echo 'extension=swoole.so' > /etc/php/7.3/mods-available/swoole.ini && ln -sf /etc/php/7.3/mods-available/swoole.ini /etc/php/7.3/cli/conf.d/

# mo templater
curl -sSL https://git.io/get-mo -o /usr/local/bin/mo
chmod +x /usr/local/bin/mo

# composer
curl -sSL https://getcomposer.org/installer -o composer-setup.php
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm -f ./composer-setup.php

source ~/.bashrc

sudo -u postgres psql -U postgres -c "DROP DATABASE IF EXISTS tgcrm"
sudo -u postgres psql -U postgres -c "CREATE DATABASE tgcrm"
sudo -u postgres psql -U postgres -c "create user vagrant with encrypted password 'vagrant'"
sudo -u postgres psql -U postgres -c "grant all privileges on database tgcrm to vagrant"

mkdir -p /home/tgcrm
cd /home/tgcrm

rm -f .env

if id vagrant >/dev/null 2>&1; then
    #ROOTLOGIN="sudo su -"
    #grep -qF -- "$ROOTLOGIN" /root/.bashrc || echo -e "\n$ROOTLOGIN\n" >> /root/.bashrc
    sudo -u www-data composer install
else
    sudo -u www-data composer install --optimize-autoloader --classmap-authoritative --no-dev  
fi

#todo force yes
php artisan october:install
php artisan october:fresh
php artisan cache:clear

if id vagrant >/dev/null 2>&1; then
    echo 'APP_ENV=dev' > .env
else
    echo 'APP_ENV=production' > .env
fi

chown www-data:www-data ./storage

# composer create-project october/october tgcrm
# php artisan plugin:refresh taraflex.tgcrm

# php artisan create:plugin taraflex.tgcrm
# php artisan create:component taraflex.tgcrm Post
# php artisan create:model taraflex.tgcrm Post
# php artisan create:controller taraflex.tgcrm Posts
# php artisan create:formwidget taraflex.tgcrm CategorySelector