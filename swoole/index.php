<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Http\Server;

['port' => $port] = getopt('p:', ['port:']);

$http = new Server('0.0.0.0', $port);

$http->on('start', function ($server) use ($port) {
    echo "Swoole http server is started at http://127.0.0.1:$port\n";
});

$http->on('request', function (Request $request, Response $response) {
    $response->header('Content-Type', 'text/plain');
    $response->end("Hello World\n");
});

$http->start();
