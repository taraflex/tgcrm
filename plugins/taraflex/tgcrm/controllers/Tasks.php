<?php namespace Taraflex\Tgcrm\Controllers {

    use BackendMenu;
    use Backend\Classes\Controller;

/**
 * Tasks Back-end Controller
 */
    class Tasks extends Controller
    {
        public $formConfig = 'config_form.yaml';

        public $implement = [
            'Backend.Behaviors.FormController',
            'Backend.Behaviors.ListController',
            'Backend.Behaviors.RelationController'
        ];

        public $listConfig = 'config_list.yaml';

        public $relationConfig = 'config_relation.yaml';

        public function __construct()
        {
            parent::__construct();

            BackendMenu::setContext('Taraflex.Tgcrm', 'tgcrm', 'tasks');
        }
    }

}
