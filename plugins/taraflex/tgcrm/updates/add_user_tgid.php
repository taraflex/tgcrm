<?php namespace Taraflex\Tgcrm\Updates;

use Illuminate\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class AddUserTgid extends Migration
{
    public function down()
    {
        Schema::table('backend_users', function(Blueprint $table) {
            $table->dropUnique('tgid');
            $table->dropColumn('tgid');
        });
    }

    public function up()
    {
        Schema::table('backend_users', function(Blueprint $table) {
            $table->unsignedBigInteger('tgid')->default(0);
            $table->unique('tgid');
        });
    }
}
