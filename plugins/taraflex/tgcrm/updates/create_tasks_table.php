<?php namespace Taraflex\Tgcrm\Updates ;
//use Illuminate\Support\Facades\DB;
    use October\Rain\Database\Schema\Blueprint;
    use October\Rain\Database\Updates\Migration;
    use October\Rain\Support\Facades\Schema;

    class CreateTasksTable extends Migration
    {
        public function down()
        {
            Schema::dropIfExists('taraflex_tgcrm_tasks');
        }

        public function up()
        {
            Schema::create('taraflex_tgcrm_tasks', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->timestamps();
                $table->text('content');
                $table->text('title');
                $table->unsignedInteger('user_id');
                $table->foreign('user_id')->references('id')->on('backend_users')->onDelete('cascade');
                $table->unsignedInteger('performer_id')->nullable();
                $table->foreign('performer_id')->references('id')->on('backend_users');
            });
        }
    }
