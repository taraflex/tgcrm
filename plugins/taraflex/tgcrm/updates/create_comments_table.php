<?php namespace Taraflex\Tgcrm\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class CreateCommentsTable extends Migration
{
    public function down()
    {
        Schema::dropIfExists('taraflex_tgcrm_comments');
    }

    public function up()
    {
        Schema::create('taraflex_tgcrm_comments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->text('content');            
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('backend_users')->onDelete('cascade');
            $table->unsignedInteger('task_id');
            $table->foreign('task_id')->references('id')->on('taraflex_tgcrm_tasks')->onDelete('cascade');
        });
    }
}