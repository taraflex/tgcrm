<?php

namespace Taraflex\Tgcrm {

    use Backend\Facades\Backend;
    use Backend\Facades\BackendAuth;
    use Illuminate\Support\Facades\Route;
    use October\Rain\Support\Facades\Url;
    use System\Classes\PluginBase;

    /**
     * Tgcrm Plugin Information File
     */
    class Plugin extends PluginBase
    {
        /**
         * Boot method, called right before the request route.
         *
         * @return array
         */
        public function boot()
        {
            Route::get('/tgauth', function () {
                $auth_data = $_GET;
                if (empty($auth_data['id'])) {
                    return response()->view('taraflex.tgcrm::tgauth', [
                        'url'          => Url::to('/tgauth'),
                        'bot_username' => config('tgbot.username')
                    ]);
                } else {
                    $check_hash = $auth_data['hash'];
                    unset($auth_data['hash']);
                    $data_check_arr = [];
                    foreach ($auth_data as $key => $value) {
                        $data_check_arr[] = $key . '=' . $value;
                    }
                    sort($data_check_arr);
                    $secret_key = hash('sha256', config('tgbot.token'), true);
                    $hash       = hash_hmac('sha256', implode("\n", $data_check_arr), $secret_key);
                    if (strcmp($hash, $check_hash) !== 0) {
                        return response()->create('Data is NOT from Telegram', 500);
                    }
                    if ((time() - $auth_data['auth_date']) > 86400) {
                        return response()->create('Telegram Data is outdated', 500);
                    }
                    $user       = BackendAuth::getUser();
                    $user->tgid = $auth_data['id'];
                    $user->save();
                    return response()->redirectTo('/backend');
                }
            })->middleware('web');
        }

        /**
         * Returns information about this plugin.
         *
         * @return array
         */
        public function pluginDetails()
        {
            return [
                'name'        => 'Tgcrm',
                'description' => 'No description provided yet...',
                'author'      => 'taraflex',
                'icon'        => 'icon-leaf'
            ];
        }

        /**
         * Register method, called when the plugin is first registered.
         *
         * @return void
         */
        public function register()
        {

        }

        /**
         * Registers any front-end components implemented in this plugin.
         *
         * @return array
         */
        public function registerComponents()
        {
            return []; // Remove this line to activate
        }

        /**
         * Registers back-end navigation items for this plugin.
         *
         * @return array
         */
        public function registerNavigation()
        {
            return [
                /*  'download_ext_crx' => [
                'label'       => 'Download extension (crx)',
                'url'         => '/download-extension-crx',
                'icon'        => 'icon-download',
                'permissions' => ['taraflex.tgcrm.*'],
                'order'       => 500
                ],*/
                'tgcrm'            => [
                    'label'       => 'Tasks',
                    'url'         => Backend::url('taraflex/tgcrm/tasks'),
                    'icon'        => 'icon-link',
                    'permissions' => ['taraflex.tgcrm.*'],
                    'order'       => 502
                ],
                'download_ext_zip' => [
                    'label'       => 'Telegram auth',
                    'url'         => '/tgauth',
                    'icon'        => 'icon-telegram',
                    'permissions' => ['taraflex.tgcrm.*'],
                    'order'       => 503
                ]
            ];
        }

        /**
         * Registers any back-end permissions used by this plugin.
         *
         * @return array
         */
        public function registerPermissions()
        {
            return []; // Remove this line to activate
            /* return [
        'taraflex.tgcrm.some_permission' => [
        'tab'   => 'tgcrm',
        'label' => 'Some permission'
        ]
        ];*/
        }
    }
}
