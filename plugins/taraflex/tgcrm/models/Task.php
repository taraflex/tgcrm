<?php namespace Taraflex\Tgcrm\Models {

    use BackendAuth;
    use Model;

    /**
     * Task Model
     */
    class Task extends Model
    {
        use \October\Rain\Database\Traits\Validation;

        public $attachMany = [];

        public $attachOne = [];

        public $belongsTo = [
            'user'      => \Backend\Models\User::class,
            'performer' => [\Backend\Models\User::class, 'key' => 'performer_id']
        ];

        public $belongsToMany = [];

        public $hasMany = [
            'comments' => \Taraflex\Tgcrm\Models\Comment::class
        ];

        /**
         * @var array Relations
         */
        public $hasOne = [];

        public $morphMany = [];

        public $morphOne = [];

        public $morphTo = [];

        /**
         * @var array Validation rules for attributes
         */
        public $rules = ['title' => 'required'];

        /**
         * @var string The database table used by the model.
         */
        public $table = 'taraflex_tgcrm_tasks';

        /**
         * @var array Attributes to be appended to the API representation of the model (ex. toArray())
         */
        protected $appends = [];

        /**
         * @var array Attributes to be cast to native types
         */
        protected $casts = [];

        /**
         * @var array Attributes to be cast to Argon (Carbon) instances
         */
        protected $dates = [
            'created_at',
            'updated_at'
        ];

        /**
         * @var array Fillable fields
         */
        protected $fillable = [];

        /**
         * @var array Guarded fields
         */
        protected $guarded = ['*'];

        /**
         * @var array Attributes to be removed from the API representation of the model (ex. toArray())
         */
        protected $hidden = [];

        /**
         * @var array Attributes to be cast to JSON
         */
        protected $jsonable = [];

        public static function boot()
        {
            parent::boot();
            self::creating(function ($model) {
                $model->user_id = BackendAuth::user()->id;
            });
        }
    }

}
