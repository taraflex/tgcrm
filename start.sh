#!/bin/bash

cd /home/tgcrm

php artisan cache:clear

export PORT=80
export DEV_PORT=3080
export TG_PORT=4080
export DEV_TG_PORT=5080
export HOSTNAME=$(php -r "\$a=[$(cat '/home/tgcrm/config/app.php' | grep 'url')];echo parse_url(end(\$a),PHP_URL_HOST);")

if id vagrant >/dev/null 2>&1; then
    systemctl stop tgtunnel 
    echo "[Unit]
Description=tgtunnel service

[Service]
Type=simple
WorkingDirectory=/home/tgcrm
ExecStart=ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o BatchMode=yes -gnNT -R $TG_PORT:localhost:$DEV_TG_PORT -i /home/tgcrm/ssh.pem root@$HOSTNAME

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/tgtunnel.service
    systemctl daemon-reload
    systemctl restart tgtunnel 
    systemctl enable tgtunnel 

    systemctl stop octobertunnel 
    echo "[Unit]
Description=octobertunnel service

[Service]
Type=simple
WorkingDirectory=/home/tgcrm
ExecStart=ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o BatchMode=yes -gnNT -R $PORT:localhost:$DEV_PORT -i /home/tgcrm/ssh.pem root@$HOSTNAME

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/octobertunnel.service
    systemctl daemon-reload
    systemctl restart octobertunnel 
    systemctl enable octobertunnel 
fi

echo "<?php return [ 'debug' => true, 'url' => 'http://dev.$HOSTNAME'];" > /home/tgcrm/config/dev/app.php

systemctl stop tgcrm 
echo "[Unit]
Description=tgcrm service

[Service]
Type=simple
WorkingDirectory=/home/tgcrm
ExecStart=php ./swoole/index.php --port=$TG_PORT

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/tgcrm.service
systemctl daemon-reload

nginx_gen () {
    rm -f /etc/nginx/sites-enabled/default

    if test -f /etc/nginx/ssl/$HOSTNAME/cert.pem && test -f /etc/nginx/ssl/$HOSTNAME/key.pem; then
        export SSL_CERT="ssl_certificate /etc/nginx/ssl/$HOSTNAME/cert.pem;" 
        export SSL_KEY="ssl_certificate_key /etc/nginx/ssl/$HOSTNAME/key.pem;" 
    else
        export SSL_CERT=""
        export SSL_KEY=""
    fi
    mo /home/tgcrm/nginx.mustache.conf > /etc/nginx/sites-available/$HOSTNAME
    ln -sf /etc/nginx/sites-available/$HOSTNAME /etc/nginx/sites-enabled/
}

nginx_gen

systemctl restart postgresql
systemctl restart php7.3-fpm
systemctl restart tgcrm
systemctl restart nginx

systemctl enable postgresql
systemctl enable php7.3-fpm
systemctl enable tgcrm 
systemctl enable nginx

if id vagrant >/dev/null 2>&1; then
    echo ''
else
    mkdir -p /etc/nginx/ssl/$HOSTNAME/
    chown -R root:root /etc/nginx/ssl/$HOSTNAME/
    chmod -R 0640 /etc/nginx/ssl/$HOSTNAME/ #read -og / write -o
    curl -sSL https://get.acme.sh | sh
    /root/.acme.sh/acme.sh --issue --nginx -d $HOSTNAME -d dev.$HOSTNAME -d tg.$HOSTNAME -d devtg.$HOSTNAME
    /root/.acme.sh/acme.sh --install-cert --force -d $HOSTNAME -d dev.$HOSTNAME -d tg.$HOSTNAME -d devtg.$HOSTNAME --key-file /etc/nginx/ssl/$HOSTNAME/key.pem --fullchain-file /etc/nginx/ssl/$HOSTNAME/cert.pem --reloadcmd "systemctl reload nginx"
    nginx_gen
fi